#ifndef TRIANGLE_H
#define TRIANGLE_H
#define DEBUG 0

#include "Shape.h"
#include <iostream>
namespace Raytracer148 {
class Triangle : public Shape {
public:
    Triangle(Eigen::Vector3d &a, Eigen::Vector3d &b ,Eigen::Vector3d &c, Eigen::Vector3d &color, int type) : a(a), b(b), c(c), color(color), type(type) {
#if DEBUG
   std::cout << "vertex a: "<< a <<std::endl;
   std::cout << "vertex b: "<< b <<std::endl;
   std::cout << "vertex c: "<< c <<std::endl;
#endif
}

    virtual HitRecord intersect(const Ray &ray);

private:
    Eigen::Vector3d a,b,c, color;
    int type;	
};
}

#endif
