#ifndef SPHERE_H
#define SPHERE_H

#include "Shape.h"

namespace Raytracer148 {
class Sphere : public Shape {
public:
    Sphere(Eigen::Vector3d &center, double radius, Eigen::Vector3d &color, int type) : c(center), r(radius), color(color), type(type) {}

    virtual HitRecord intersect(const Ray &ray);

private:
    Eigen::Vector3d c, color;
    double r;
    int type;
};
}

#endif
