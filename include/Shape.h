#ifndef SHAPE_H
#define SHAPE_H

#include <Eigen/Dense>
enum type{KDIFFUSE, KDIFFUSE_REFLECT};

namespace Raytracer148 {
struct Ray {
  Eigen::Vector3d origin, direction;
};

class Shape;

struct HitRecord {
  Eigen::Vector3d position, normal, objectcolor;
  double t;
  int type;
};

class Shape {
public:
  virtual ~Shape(){}
  virtual HitRecord intersect(const Ray &ray) = 0;
};

}

#endif
