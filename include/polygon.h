#ifndef POLY_H
#define POLY_H
#define DEBUG 0

#include "Shape.h"
#include "Triangle.h"
#include <iostream>
namespace Raytracer148 {
class Polygon : public Shape {
public:
   Polygon(Eigen::Vector3d &a, Eigen::Vector3d &b ,Eigen::Vector3d &c, Eigen::Vector3d &d):a(a), b(b), c(c), d(d) {
   T1= new Triangle(a,b,c);
   T2= new Triangle(b,c,d);
}
   // void Render_Poly(const Ray &ray);
   virtual HitRecord intersect(const Ray &ray);
    

private:
   Eigen::Vector3d a,b,c,d;	
   Triangle* T1;
   Triangle* T2;
};
}

#endif
