#include "Scene.h"
#include "Shape.h"
#include <cmath>
#include <iostream>
#define ANTIALIASING 0
using namespace Raytracer148;
using namespace std;
using namespace Eigen;

int depth = 1;
int max_depth = 5;
HitRecord Scene::closestHit(const Ray &ray) {
    HitRecord result;
    result.t = -1;
    bool foundSomething = false;

    for (unsigned int i = 0; i < shapes.size(); i++) {
        HitRecord r = shapes[i]->intersect(ray);
        if (r.t > std::numeric_limits<double>::epsilon() && (!foundSomething || r.t < result.t)) {
            result = r;
            foundSomething = true;
        }
    }

    return result;
}

// Returns an RGB color, where R/G/B are each in the range [0,1]
Vector3d Scene::trace(const Ray &ray,int depth) {
    //if(depth > max_depth) 
    Vector3d As; As[0] = 0; As[1] = 0; As[2] = 0;
    Vector3d Al; Al[0] = 0.3; Al[1] = 0.3; Al[2] = 0.3;
    Vector3d Dl; Dl[0] = 0.8; Dl[1] = 0.8; Dl[2] = 0.8;
    Vector3d Sl; Sl[0] = 1.0; Sl[1] = 1.0; Sl[2] = 1.0;
    //Vector3d Am; Am[0] = 1.0; Am[1] = 0.5; Am[2] = 0.3;
    Vector3d Dm; Dm[0] = 0.9; Dm[1] = 0.5; Dm[2] = 0.5;
    Vector3d Sm; Sm[0] = 0.8; Sm[1] = 0.8; Sm[2] = 0.8;
    // For now we'll have diffuse shading with no shadows, so it's easy!
    //HitRecord r = closestHit(ray);
    if(depth > max_depth) return As;
    Vector3d result;
     
	HitRecord r = closestHit(ray);
	Vector3d Am = r.objectcolor;
        //Vector3d result;
        result[0] = result[1] = result[2] = 0;

	if (r.t < numeric_limits<double>::epsilon()) return result;
	Vector3d lightDir = (lightPos - r.position).normalized();
	double dot = lightDir.dot(r.normal); 
    	result[0] = (As[0]*Am[0])+(Al[0]*Am[0]);
	result[1] = (As[1]*Am[1])+(Al[1]*Am[1]);
	result[2] = (As[2]*Am[2])+(Al[2]*Am[2]);
    
	if (dot > 0)
	{	
	    result[0] += Dl[0]*Dm[0]*dot;
	    result[1] += Dl[1]*Dm[1]*dot;
	    result[2] += Dl[2]*Dm[2]*dot;
	    Vector3d rev_light;
	    Vector3d normal = r.normal;
	    Vector3d reflect = lightDir - 2 * lightDir.dot(normal) * normal;
	    double min = 0;
	    double rDote = max(min, reflect.dot(-lightDir));	    
	    
	    double specular = pow(rDote,10.0);
	    
	    if(specular > 0.0)
	    
	    result[0] += Sl[0]*Sm[0]*specular;
	    result[1] += Sl[1]*Sm[1]*specular;
	    result[2] += Sl[2]*Sm[2]*specular;
	    Ray shadow_ray;
	//r.normal[0] *= 0.001;
	//r.normal[1] *= 0.001;
	//r.normal[2] *= 0.001;
	
	    shadow_ray.origin = r.position+r.normal;
	    shadow_ray.direction = (lightPos-r.position).normalized(); 
	    HitRecord shadow = closestHit(shadow_ray);
	    if(shadow.t < numeric_limits<double>::epsilon())
	    {
		if(r.type == KDIFFUSE_REFLECT)
		{
		    Ray reflected_ray;
		    reflected_ray.origin= r.position;
		    reflected_ray.direction = ray.direction - 2 * ray.direction.dot(r.normal) * r.normal;
		    Vector3d reflected_color = trace(reflected_ray, depth+1);
		    result+=reflected_color;
		}
	        return result;
	    }
		//return result;
	    result[0] *= 0;
	    result[1] *= 0;
	    result[2] *= 0;
        }
	 
	    
	//}
	return result;
    
}

void Scene::render(Image &image) {
    // We make the assumption that the camera is located at (0,0,0)
    // and that the image plane happens in the square from (-1,-1,1)
    // to (1,1,1).

    assert(image.getWidth() == image.getHeight());
    //Vector3d color;
    int size = image.getWidth();
    double pixelSize = 2. / size;
    for (int x = 0; x < size; x++)
        for (int y = 0; y < size; y++) {
	    Vector3d color;
#if ANTIALIASING 
	    for(int p = 0; p<3; p++)
	    {
		for(int q = 0; q<3;q++)
		{
		    Ray curRay;
		    curRay.origin[0] = curRay.origin[1] = curRay.origin[2] = 0;
		    curRay.direction[0] = ((x + 0.5) * pixelSize - 1)+(p+0.5)/3;
		    curRay.direction[1] = 1 - ((y + 0.5) * pixelSize)+(q+0.5)/3;
		    curRay.direction[2] = 1;
		    color = trace(curRay,depth);
		}
	    }
#else
    Ray curRay;
    curRay.origin[0] = curRay.origin[1] = curRay.origin[2] = 0;
    curRay.direction[0] = ((x + 0.5) * pixelSize - 1);
    curRay.direction[1] = 1 - ((y + 0.5) * pixelSize);
    curRay.direction[2] = 1;
    color = trace(curRay,depth);

	//cout<<"value of color[0]"<<color[0]<<endl;
	//cout<<"value of color[1]"<<color[1]<<endl;
	//cout<<"value of color[2]"<<color[2]<<endl;
	
#endif
            image.setColor(x, y, color[0], color[1], color[2]);
        }
}
