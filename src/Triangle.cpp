
#include "Triangle.h"
#include <limits>
#include <iostream>
using namespace Raytracer148;
using namespace Eigen;
using namespace std;

HitRecord Triangle::intersect(const Ray &ray) {
   HitRecord result;
   
   result.objectcolor = color;
   result.type = type;

#if DEBUG
   std::cout<<"The origin of ray under consideration: "<<ray.origin<<std::endl;
   std::cout<<"The direction of ray under consideration: "<<ray.direction<<std::endl;

   std::cout << "vertex a: "<< a <<std::endl;
   std::cout << "vertex b: "<< b <<std::endl;
   std::cout << "vertex c: "<< c <<std::endl;
#endif 
   //when the ray does not hite triangle, the result.t=-1
   result.t = -1;
   // compute plane's normal
   Vector3d ab = b - a;
   Vector3d bc = c - b;
   Vector3d ac = c - a;
   Vector3d ca = a - c; 
    // no need to normalize
   Vector3d N = ab.cross(ac); // N  
   #if DEBUG
      std::cout<<"Normal of the triangle: "<<N<<std::endl;
   #endif
   // Step 1: finding P
 
    // check if ray and plane are parallel ?
    float NdotRayDirection = N.dot(ray.direction); 
   if (fabs(NdotRayDirection) <  numeric_limits<double>::epsilon()) // almost 0 
   {
   #if DEBUG
      std::cout<<"the ray is parallel to the triangle"<<std::endl;
   #endif
      return result; // they are parallel so they don't intersect ! 
   }
  
   float d = N.dot(a);  
    // compute t (equation 3)
   double t = (N.dot(ray.origin) + d) / NdotRayDirection; 
   Vector3d P = ray.origin + t * ray.direction;
    // check if the triangle is in behind the ray
   if (t < 0)
   {
   #if DEBUG
      std::cout<<"the triangle is behind"<<std::endl;
   #endif
    return result; // the triangle is behind 
   }
    // Step 2: inside-outside test
   Vector3d C; // vector perpendicular to triangle's plane 
 
    // edge 0
    //Vector3d edge0 = v1 - v0; 
   Vector3d vp0 = P - a; 
   C = ab.cross(vp0); 
   if (N.dot(C) < 0) 
   {
   #if DEBUG
      std::cout<<"P is on the right side of ab"<<std::endl;
   #endif
   return result; // P is on the right side 
   }
    // edge 1 
    Vector3d vp1 = P - b; 
    C = bc.cross(vp1); 
    //u = C.norm() / area2; 
   if (N.dot(C) < 0)
   {
   #if DEBUG
      std::cout<<"P is on the right side of bc"<<std::endl;
   #endif
   return result; // P is on the right side 
   }
 
    // edge 2
//    Vector3d edge2 = v0 - v2; 
    Vector3d vp2 = P - c; 
    C = ca.cross(vp2);
    //v = C.norm() / area2; 
   if (N.dot(C) < 0)
   {
   #if DEBUG
      std::cout<<"P is on the right side of ca"<<std::endl;
   #endif
   return result; // P is on the right side;
   } 
   else{
    result.t = t;
      result.position =  P;
      result.normal = -N.normalized(); 
   }
    return result; // this ray hits the triangle 
   
}
