
#include "Triangle.h"
#include "polygon.h"
#include <limits>
#include <iostream>
using namespace Raytracer148;
using namespace Eigen;
using namespace std;

HitRecord Polygon::intersect(const Ray &ray) {
   HitRecord result = T1->intersect(ray);
   result = T2->intersect(ray);

   return result; // this ray hits the triangle   
}
