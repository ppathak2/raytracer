#include <iostream>
#include <fstream>
#include "Image.h"
#include "Scene.h"
#include "Sphere.h"
#include "Triangle.h"
#include "Ellipsoid.h"
#include "Shape.h"
//#include "polygon.h"
using namespace std;
using namespace Raytracer148;
using namespace Eigen;

int main() {
    Image im(1000, 1000);

    Scene scene;
    Vector3d spherecolor;
    spherecolor[0] = 1;
    spherecolor[1] = 0;
    spherecolor[2] = 0;

    Vector3d center;
    center[0] = 0;
    center[1] = 0;
    center[2] = 4;
    scene.addShape(new Sphere(center, 2,spherecolor,KDIFFUSE));

    center[0] = -.5;
    center[1] = 1.0;
    center[2] = 2.5;
    scene.addShape(new Sphere(center, .5,spherecolor,KDIFFUSE));

    center[0] = 0.5;
    center[1] = 1.25;
    center[2] = 2.75;
    scene.addShape(new Sphere(center, .5,spherecolor, KDIFFUSE));
    Vector3d cubecolor;
    spherecolor[0] = 0;
    spherecolor[1] = 0;
    spherecolor[2] = 1;
    //Vector3d center;
    center[0] = -2.5;
    center[1] = -2;
    center[2] = 3.5;
    scene.addShape(new Sphere(center, .5,spherecolor, KDIFFUSE));

    cubecolor[0] = 0.3;
    cubecolor[1] = 0.3;
    cubecolor[2] = 0.3;

    Vector3d t1, t2, t3, t4,t5,t6;
    t1[0] = -8;
    t1[1] = -2.5;
    t1[2] = 8;
    t2[0] = -8;
    t2[1] = -2.5;
    t2[2] = 1;
    t3[0] = 8;
    t3[1] = -2.5;
    t3[2] = 1;
    t4[0] = 8;
    t4[1] = -2.5;
    t4[2] = 1;
    t5[0] = 8;
    t5[1] = -2.5;
    t5[2] = 8;
    t6[0] = -8;
    t6[1] = -2.5;
    t6[2] = 8;
    scene.addShape(new Triangle(t1,t2,t3,cubecolor,KDIFFUSE_REFLECT));
    scene.addShape(new Triangle(t4,t5,t6,cubecolor,KDIFFUSE_REFLECT));
    cubecolor[0] = 0;
    cubecolor[1] = 1;
    cubecolor[2] = 0;
    //Vector3d t1, t2, t3;
    ifstream myReadFile;
    string line;
    myReadFile.open("vertices.txt");
    cout<<"Reading vertices.txt"<<endl;
    if (!(myReadFile.is_open()))
    {
	cout <<"No file found";
    }
    while (!myReadFile.eof())
    {
	string v1x,v1y,v1z;
	string v2x,v2y,v2z;
	string v3x,v3y,v3z;
	myReadFile >> v1x>>v1y>>v1z;
	myReadFile >> v2x>>v2y>>v2z;
	myReadFile >> v3x>>v3y>>v3z;

        t1[0] = atof(v1x.c_str());
        t1[1] = atof(v1y.c_str());
        t1[2] = atof(v1z.c_str());
	t2[0] = atof(v2x.c_str());
        t2[1] = atof(v2y.c_str());
        t2[2] = atof(v2z.c_str());
	t3[0] = atof(v3x.c_str());
	t3[1] = atof(v3y.c_str());
	t3[2] = atof(v3z.c_str());
	
    scene.addShape(new Triangle(t1,t2,t3,cubecolor,KDIFFUSE));
   
    }
   
    myReadFile.close();    
    scene.render(im);

    im.writePNG("test.png");

    return 0;
}
